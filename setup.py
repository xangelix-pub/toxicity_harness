from setuptools import setup, find_packages

setup(
    name='toxicity_harness',
    version='0.1',
    packages=find_packages(),
    description='An experimental LLM toxicity harness.',
    url='https://gitlab.com/xangelix-pub/toxicity_harness',
    author='Cody Wyatt Neiman',
    author_email='neiman@cody.to',
    license='GPLv3',
    install_requires=[
        "transformers",
        "datasets",
        "evaluate",
        "optimum",
        "auto_gptq",
        "matplotlib",
        "tqdm",
        "numpy",
        "Pillow",
        "torch"
    ],
    zip_safe=False
)
